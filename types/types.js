//typeof is a operator
var a; // undefined
var b = undefined; //undefined

var c = false; // boolean
var d = true; //boolean

var e = 1;  //number
var f = 4 * "hola"; // (Nan) number
var g = 4.1 * "hola"; // (Nan) number
var h = 1.5; //number

var i = ""  // () string
var j = "palabra"; // string
var k = '*'; //string
var l = 5 + "hola"; // (5hola) string
var m = 5.1 + "hola"; // (5,1hola) string


var n = function (){ console.log("hola")}; //function
var o = async function (){ console.log("hola dos")}; //function

var p = null; // (null) object
var q = []; //object
var r = new Array("Saab", "Volvo", "BMW"); //object
var s = {}; //object
var t = {"a" : "b"}; //object
var u = {"a" : "b","c" : "x"}; //object
var v = [0,1]; //object
var w = [0,"a"]; //object
var x = new Date() //object

console.log(h); //undefined
console.log(typeof h);

console.log(typeof noexiste); //undefined

console.log(typeof Math === 'object') //true
console.log(typeof Date === 'function') //true
console.log(typeof Function === 'function') //true
console.log(typeof Object === 'function') //true
console.log(typeof String === 'function') //true
console.log(null===undefined) //false
console.log(null==undefined) //true

console.log(""===null) //false
console.log(""==null) //false
// the types in javascript are: undefined, boolean, number, string, object
// float is a string
// char is a string
// array is a object