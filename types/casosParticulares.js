var f = 4 * "hola"; // (Nan) number
var g = 4.1 * "hola"; // (Nan) number
var h = 1.5; //number

var p = null; // (null) object
var r = new Array("Saab", "Volvo", "BMW"); //object
var v = [0,1]; //object
console.log(typeof Math === 'object') //true


var k = '*'; //string

console.log(typeof Date === 'function') //true
console.log(typeof Function === 'function') //true
console.log(typeof Object === 'function') //true
console.log(typeof String === 'function') //true

console.log(null===undefined) //false
console.log(null==undefined) //true

console.log(""===null) //false
console.log(""==null) //false


console.log (p);
console.log(typeof p);

