var a; // 
var b = undefined; //

var c = false; // 
var d = true; //

var e = 1;  //
var f = 4 * "hola"; //
var g = 4.1 * "hola"; //
var h = 1.5; //

var i = ""  //
var j = "palabra"; //
var k = '*'; //
var l = 5 + "hola"; //
var m = 5.1 + "hola"; //


var n = function (){ console.log("hola")}; //
var o = async function (){ console.log("hola dos")}; //

var p = null; // 
var q = []; //
var r = new Array("Saab", "Volvo", "BMW"); //
var s = {}; //
var t = {"a" : "b"}; //
var u = {"a" : "b","c" : "x"}; //
var v = [0,1]; //
var w = [0,"a"]; //
var x = new Date() //

console.log(h); //
console.log(typeof h);

console.log(typeof noexiste); //

console.log(typeof Math === 'object') //
console.log(typeof Date === 'function') //
console.log(typeof Function === 'function') //
console.log(typeof Object === 'function') //
console.log(typeof String === 'function') //
console.log(null===undefined) //
console.log(null==undefined) //

console.log(""===null) //
console.log(""==null) //