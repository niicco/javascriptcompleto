var a = "var a"
let b = "let b"
const c = "const c"
// console.log("before function" + f); //carName is not defined
function myFunction() {
    var d = "var d";
    let e = "let e";
    const f = "const e";
    if(true){
        var g = "var g";
        let h = "let h";
        const i = "const i";
    }
    
    console.log("a in function " + a);
    console.log("b in function " + b);
    console.log("c in function " + c);
    console.log("d in function " + d);
    console.log("e in function " + e);
    console.log("f in function " + f);
    console.log("g in function " + g);
    // console.log("h in function " + h); //h is not defined
    // console.log("i in function " + i); //i is not defined
  }
  myFunction()
  console.log("a after function " + a);
  console.log("b after function " + b);
  console.log("c after function " + c);
  // console.log("d after function " + d); //d is not defined
  // console.log("e in function " + e); //e is not defined
  // console.log("f in function " + f); //f is not defined
  // console.log("g in function " + g); //g is not defined

  //si es var dentro de la funcion no puedo acceder desde afuera
  //si es var dentro de un condicionador  que está en una función, puedo acceder desde esa misma función