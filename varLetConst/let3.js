function pruebaLet(){
  let y=20
  if (true){
    let y=30
    console.log(y) //muestra el let con el cambio
  }
  console.log(y) //muestra el let sin el cambio
}

pruebaLet();
// console.log(y); //da error porque está fuera del scope
