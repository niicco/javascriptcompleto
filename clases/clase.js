'use strict'

class Persona{
    constructor(nombre, apellido){
        this.nombre=nombre;
        this.apellido =apellido
    }
    full_name(){ //metodo de la clase
        return this.nombre + " " +this.apellido;
    }
    static definicion(){
        console.log("una persona es un ser humano")
    }
}
var user = new Persona("uriel", "hernandez");
console.log(user.full_name())
console.log(Persona.definicion())