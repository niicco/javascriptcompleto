class Player{
    play(){
        this.video.play();
    };
    duration(){
        return this.video.duration/100;
    }
}
class Vimeo extends Player{
    open(){
        this.redirectToVimeo(this.video);
    }
}
class Youtube extends Player{
    open(){
        this.redirectToYoutube(this.video);
    }
}
