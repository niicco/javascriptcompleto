//está mal si se quiere ocultar el precio y que no se pueda modificar desde afuera
/*
function Car(_doors){
    this.doors = _doors;
    this.price = 1200;
    this.getPrice = function(){
        return this.doors * this.price
    }
}
*/
function Car(_doors){
    let doors = _doors;
    let price = 1200;
    this.getPrice = ()=> {return doors * price}
}
const ferrari= new Car (4);
console.log(ferrari.getPrice())