function Circle(radius){
    this.radius = radius;
}
const circle1 = new Circle(2);
console.log(circle1.constructor) // [Function: Circle]


function createAnimal(){
    return {
        name: 'Bob'
    }
}
const cat= createAnimal();
console.log(cat)
console.log(cat.constructor) // [Function: Object]