function createDog(name){
    return{
        name, //crea una propiedad con un valor con el mismo nombre es igual a hacer name: name
        run: ()=> console.log('correr')
    }
}
const chiguagua = createDog('Boby')
console.log(chiguagua.run())