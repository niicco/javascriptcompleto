function Tutor(nombre, apellido){
    this.nombre =nombre;
    this.apellido=apellido;
}

let user =new Tutor('nico', 'aguero')
console.log(user.__proto__) //Tutor {}
console.log(typeof user.__proto__) //object 
console.log(user.__proto__.__proto__) //{}

console.log(user.nombre) // nico
console.log(Tutor.prototype) //Tutor {}

