const mySet = new Set();
console.log(mySet)
mySet.add(1);
mySet.add(1);
mySet.add(1);
mySet.add(true);
mySet.add(true);
mySet.add(false);
mySet.add(()=>1);
mySet.add('hola');
mySet.add(()=>1);
console.log(mySet) // Set { 1, true, false, [Function], 'hola', [Function] }
console.log(mySet.values())
// mySet.forEach((e)=>{console.log(e)})
console.log(mySet.has('hola'))