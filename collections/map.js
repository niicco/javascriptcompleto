const a=[1,2,3,4,5,6]

const b =new Map();
b.set(true, 'juan')
b.set(true, 'juan')
b.set('color', 'red')
console.log(b) // Map { true => 'juan' }
console.log(b.size) // 2
console.log(b.entries()) // [Map Entries] { [ true, 'juan' ] }
console.log(typeof b) //object
// b.delete(true) //lo borra
b.forEach(element=>{ //muestra solamente los values de la coleccion map
    console.log(element)
})
console.log(b.values()) //
b.has('color') // devuelve true
// b.clear() // limpia todo el map