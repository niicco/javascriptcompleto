/* 
Estos valores son truthy
true
'false'
{}
'string'
3.14
new Date ()

Estos valores son falsy:
false
0
''
null
undefined
NaN
*/
let a =!!(0) //false
let b = !!('0') //true
let c= (false==0)
let d=(false=='')
let e= (0=='')

// los valores falsy null y undefined no son equivalentes a nada, excepto a sí mismos y entre sí
let f =(null== false) //false
let g=(null ==null) //true
let h= (undefined ==undefined) //true
let i= (undefined==null) //true

//el valor falsy NaN no es equivalente a nada - incluyendo el mismo NaN
let j = (NaN ==null); //false
let k= (NaN == NaN) //false

let l =(false==0) //true
console.log(f)