console.log(typeof 0); //number
console.log(!0); //true
console.log(typeof undefined) //undefined
console.log(!undefined) // true
console.log(!null) //true
console.log(!1) //false
if(NaN){
    console.log("no se muestra")
}
if(null){
    console.log("no se muestra")
}
if(""){
    console.log("no se muestra")
}
if("string"){
    console.log("se muestra")
}
if({}){
    console.log("se muestra")
}