console.log(typeof 0); //number
console.log(!0); //true
console.log(typeof undefined) //undefined
console.log(!undefined) //true
console.log(!null) //true
console.log(!1) //false
console.log(-true) // -1
console.log( -"3") // -3
console.log(2**3) //8
