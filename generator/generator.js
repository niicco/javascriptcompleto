function* gen(){
    yield 1;
    yield 2;
    yield 3;
}
let g=gen();
console.log(g) //Object [Generator] {}
console.log(g.next()) //{ value: 1, done: false }
console.log(g.throw()) //lanza un error