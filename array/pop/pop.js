// elimina el último elemento de un array y lo devuelve
a = ['uno','dos','tres','cuatro','cinco']
console.log(a.pop()) // cinco
a.pop()
console.log(a) // ['uno','dos','tres']