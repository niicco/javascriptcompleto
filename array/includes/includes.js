const charly = {name: 'charly'}
const animals= ['dog', 'cat', 'cow', 'fox', {name: 'bob'}, charly];

const exist = animals.includes('horse');

console.log(exist); //false
console.log(animals.includes({name: 'bob'})); //false
console.log(animals.includes(charly)); // true