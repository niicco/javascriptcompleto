//crea un solo array a partir del valor que recibe
console.log(Array.of(1)); //[1]
console.log(Array.of('foo')); //[ 'foo' ]
console.log(Array.of(1,2,3)) //[1,2,3]
console.log(Array.of(undefined)) //[undefined]