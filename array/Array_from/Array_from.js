// Array.from() crea una instancia Array a partir de un objeto iterable
console.log(Array.from('foo')); //[ 'f', 'o', 'o' ]
console.log(Array.from([1,2,3], x => x +20)); //[ 21, 22, 23 ]