const vowels = ['a','e','i','o','u']

const iterator = vowels.entries();
console.log(iterator) //Object [Array Iterator] {}
console.log(iterator.next().value) // [ 0, 'a' ]
console.log(iterator.next().value) // [ 1, 'e' ]
console.log(iterator.next().value) // [ 2, 'i' ]