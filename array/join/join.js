//join crea un string a partir de un array usando como separador el argumento que se recibe
let languages = ['javascript', 'php','java']
console.log(languages.join()) // javascript,php,java
console.log(languages.join(' ')) // javascript php java
//object join string