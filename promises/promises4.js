let promesa = new Promise((resolve, reject)=>{
    setTimeout(()=>{
        resolve('promesa resuelta correctamente')
    }, 2000);
    setTimeout(()=>{
        reject('Ocurrió un error');
    },3000);
});
promesa.then((response)=>{ 
    console.log('response ', response);
}).catch((error)=>{
    console.log('error: ', error);
})