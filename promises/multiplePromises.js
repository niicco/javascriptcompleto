let p1 =new Promise((resolve, reject)=>setTimeout(resolve, 500, 'hola mundo'));

let p2 =new Promise((resolve, reject)=>setTimeout(resolve, 1000, 'hola mundo'));

let p3= Promise.reject(); //promesa con el estado reject

let saluda =()=> console.log('hola a todos')

/*
p1.then(()=>{
    p2.then(()=>saluda())
});
*/

//promise.all se ejecuta si todas las promesas son fullfiled
Promise.all([p1, p2, p3]).then(resultados=> {
    console.log(resultados);
    saluda()
}).catch(()=>console.log('fallé'))
 //promise.all recibe un iterabel

//falla si solamente una falla