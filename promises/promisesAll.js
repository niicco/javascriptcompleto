let Promesa1= new Promise((resolve,reject)=>{
    resolve('primera promesa resuleta');
})
let Promesa2= new Promise((resolve,reject)=>{
    setTimeout(()=>{
        resolve('segunda promesa resuleta');
    },500);
});
let Promesa3= new Promise((resolve,reject)=>{
    setTimeout(()=>{
        reject('error fatal en la tercera promesa')
    },1000);
});
let Promesa4= new Promise((resolve,reject)=>{
    setTimeout(()=>{
        resolve('cuarta promesa resuleta');
    },1500);
});

Promise.all([Promesa1, Promesa2, Promesa3, Promesa4])
    .then((values)=>{
        console.log('los valores son', values)
    })
    .catch((error)=>{
        console.log('ocurrió un error', error)
    });