
let Promesa2= new Promise((resolve,reject)=>{
    setTimeout(()=>{
        resolve('segunda promesa resuleta');
    },100);
});
let Promesa3= new Promise((resolve,reject)=>{
    setTimeout(()=>{
        resolve('tercera promesa')
    },500);
});
let Promesa4= new Promise((resolve,reject)=>{
    setTimeout(()=>{
        resolve('cuarta promesa resuleta');
    },1500);
});

Promise.race([Promesa2, Promesa3, Promesa4])
    .then((values)=>{
        console.log('los valores son', values)
    })
    .catch((error)=>{
        console.log('ocurrió un error', error)
    });