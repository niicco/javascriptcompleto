console.log(this) //{}

function demo(){
    console.log(this);
}
//demo();

let objeto ={
    demo: function (){ console.log(this)} //{ demo: [Function: demo] }
}
//objeto.demo()

let executor = {
    funcion: null,
    execute: function(f){
        this.funcion =f;
        this.funcion();
    }
}

executor.execute(objeto.demo)