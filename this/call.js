function executor(funcion){
    funcion.call(tutor);
}
let tutor = {
    nombre: 'Uriel',
    apellido: 'Hernandez',
    nombreCompleto: function (){
        console.log(this.nombre + ' '+ this.apellido)
    }
}
executor(tutor.nombreCompleto) //Uriel Hernandez