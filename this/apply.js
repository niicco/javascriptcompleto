function executor(funcion){
    funcion.apply(tutor);
}
let tutor = {
    nombre: 'Uriel',
    apellido: 'Hernandez',
    nombreCompleto: function (){
        console.log(this.nombre + ' '+ this.apellido)
    }
}
tutor.nombreCompleto() // Uriel Hernandez
executor(tutor.nombreCompleto) //udefined undefined