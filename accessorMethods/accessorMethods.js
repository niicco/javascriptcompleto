class Usuario{
    constructor(nombre){
        this.nombre =nombre;
    }
    get name(){
        return this.nombre.charAt(0).toUpperCase()+ this.nombre.slice(1); //Hace que la primer letra se mayúscula
    }
}
let user = new Usuario('marcos');

console.log(user.name); //utiliza el get para devolver el nombre
