let user = {
    nombre: 'Juan',
    apellido: 'Martinez',
    edad_valor: 22,
    getTutor: function(){
        return this.apellido; // getter
    },
    get edad(){
        return this.edad_valor
    }

}
console.log(user.getTutor()) //Martinez
console.log(user.edad)
console.log(typeof user.edad)
console.log(user)